from aiogram.dispatcher import FSMContext
import sqlite3

from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types
from loader import dp, bot
from keyboards import fmarkup, fbtn_2, fbtn_3, fbtn_1, fbtn_4
from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData
from database import set_film, chek_film

class Poll(StatesGroup):
    poll = State()

@dp.message_handler(commands='poll', state='*')
async def poll_command(msg: types.Message, state: FSMContext):
    chek_film(msg.from_user.id)
    await msg.answer('выберите жанры фильмов, которые вам нравятся:', reply_markup=fmarkup)
    await state.set_state(Poll.poll.state)
    await state.update_data(kb=[fbtn_3, fbtn_2, fbtn_1])

@dp.callback_query_handler(text=['fbtn_fantasy', 'fbtn_drama', 'fbtn_comedia'], state=Poll.poll)
async def poll_callback(call: types.CallbackQuery, state: FSMContext):
    await call.answer('нажата кнопка')
    kb = (await state.get_data('kb')).get('kb')
    txt = call.data
    set_film(txt, call.from_user.id)
    for i in kb:
        if i.callback_data == txt:
            kb.remove(i)
    await state.update_data(kb=kb)
    await call.message.edit_text('выберите жанры фильмов, которые вам нравятся:', reply_markup=InlineKeyboardMarkup(row_width=1).add(*kb).add(fbtn_4))

@dp.callback_query_handler(text='close', state=Poll.poll)
async def poll_close_callback(call: types.CallbackQuery, state: FSMContext):
    await call.answer('нажата кнопка')
    await state.finish()
    await call.message.edit_text('вы проголосовали, спасибо')

