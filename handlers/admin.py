
from aiogram.dispatcher import FSMContext
import sqlite3
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types
from loader import dp, bot

class AdminMsg(StatesGroup):
    admtext = State()

@dp.message_handler(commands=['admin_msg'])
async def command_admin(msg: types.Message, state: FSMContext):
    if msg.from_user.id == 715480472:
        await AdminMsg.admtext.set()
        await msg.answer('введите сообщение рассылки')
    else:
        await msg.answer('вы не админ!')


@dp.message_handler(state=AdminMsg.admtext)
async def admin_msg(msg: types.Message, state: FSMContext):
    await state.finish()
    mess = msg.text
    with sqlite3.connect('database.db') as connection:
        cur = connection.cursor()
        member_string = cur.execute("""SELECT * from user""").fetchall()
        print(member_string)
        for row in member_string:
            member_id = row[0]
            try:
                await bot.send_message(chat_id=member_id, text=mess)
            except:
                print('bot blocked by user')
        connection.commit()
    await msg.answer('Сообщение успешно отправилось всем пользователям)')
