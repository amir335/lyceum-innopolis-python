from aiogram import types
from random import choice
from loader import dp

@dp.message_handler(text='попытка(try)')
async def button_try(msg: types.Message):
    tr = choice([True, False])
    if tr:
        await msg.reply('Успех💪')
    else:
        await msg.reply('Неудача😔')