from aiogram import types

from filters.custom import IsAdmin, IsGroup
from loader import dp

@dp.message_handler(IsAdmin(), IsGroup(), commands='ban')
async def kick_handler(msg: types.Message):
    try:
        s_msg = msg.reply_to_message
    except:
        await msg.answer('сообщение не является ответом')
        return
    try:
        await msg.chat.kick(s_msg.from_user.id)
        await msg.answer(f'{s_msg.from_user.full_name} was kicked by admin')
    except:
        await msg.answer('admin cant be deleted')