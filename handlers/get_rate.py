from aiogram import types
from loader import dp, bot
import pprint
import requests

@dp.message_handler(commands='get_rate')
async def poll_command(msg: types.Message):
    num = requests.get('https://www.cbr-xml-daily.ru/daily_json.js').json()['Valute']['CNY']['Value']
    await msg.answer(f'Юань стоит {num} рублей!')