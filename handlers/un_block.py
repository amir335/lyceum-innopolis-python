import sqlite3
import logging
from aiogram import types
from loader import dp

@dp.message_handler(commands=['un_block'])
async def to_black_list(msg: types.Message):
    await msg.answer('Разбан')
    with sqlite3.connect('database.db') as connection:
        cur = connection.cursor()
        user = cur.execute(f"SELECT * FROM black_list WHERE user_id={msg.from_user.id}").fetchone()
        if user:
            logging.info(f'удаление пользователя из чс: {msg.from_user.username} {msg.from_user.id}')

            cur.execute(f"DELETE FROM black_list WHERE user_id={msg.from_user.id}").fetchone()

            connection.commit()