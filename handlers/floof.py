from loader import dp
from aiogram import types
import requests


@dp.message_handler(text='🦊floof🦊')
async def button_floof(msg: types.Message):
    response = requests.get('https://randomfox.ca/floof/').json().get('image')
    await msg.answer_photo(response)