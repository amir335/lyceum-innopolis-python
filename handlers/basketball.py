import asyncio
from aiogram import types
from loader import dp

@dp.message_handler(text='🏀basketball🏀')
async def button_basket(msg: types.Message):
    ball = await msg.answer_dice('🏀')
    await asyncio.sleep(3.5)
    if ball['dice']['value'] > 3:
        await msg.reply('отличный бросок!')
    else:
        await msg.reply('не расстраивайся, еще попадешь!')