from aiogram.dispatcher import FSMContext
import sqlite3
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types
from loader import dp


class Repeat(StatesGroup):
    state1 = State()
    state2 = State()



@dp.message_handler(text='🔄спам(повтор)🔄')
async def button_repeat(msg: types.Message, state: FSMContext):
    await Repeat.state1.set()
    await msg.reply('введите строчку, которую нужно повторить:')


@dp.message_handler(state=Repeat.state1)
async def button_repeat1(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['string'] = msg.text

    await Repeat.next()
    await msg.reply('введите количество повторений:')


@dp.message_handler(state=Repeat.state2)
async def button_repeat2(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        string = data['string']

    text = ''
    count_repeat = msg.text
    try:
        for i in range(1, int(count_repeat) + 1):
            text += f'{i}. {string}\n'
        await msg.answer(text)
    except Exception as error:
        await msg.answer('Вы что-то ввели не так! Можете попробовать снова, нажав на кнопку повторно')
    await state.finish()