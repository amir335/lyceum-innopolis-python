from aiogram import types
import config
from loader import dp
from handlers.payments.items import basic, premium, vip
from keyboards import subs_markup
from peewee_db import *

@dp.message_handler(commands='menu')
async def menu_cmd(msg: types.Message):
    await msg.answer('Проверить подписку /chek\nвыберите тип подписки:', reply_markup=subs_markup)

@dp.callback_query_handler(text='vip')
async def vip_call(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **vip.create_invoices(), provider_token=config.PROVIDER_UKASSA_TOKEN, payload='vip')

@dp.callback_query_handler(text='premium')
async def vip_call(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **premium.create_invoices(), provider_token=config.PROVIDER_UKASSA_TOKEN, payload='premium')

@dp.callback_query_handler(text='basic')
async def vip_call(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **basic.create_invoices(), provider_token=config.PROVIDER_UKASSA_TOKEN, payload='basic')

@dp.message_handler(content_types=types.ContentType.SUCCESSFUL_PAYMENT)
async def process_pay(msg: types.Message):
    payload = msg.successful_payment.invoice_payload
    with db:
        try:
            Sub(id=msg.from_user.id, subscribe=payload).save()
        except:
            await msg.answer('вы больше не можете покупать подписки')

@dp.message_handler(commands='chek')
async def chek_sub(msg: types.Message):
    with db:
        status = Sub.get(Sub.id == msg.from_user.id)
        await msg.answer(status.subscribe)
