from aiogram.types import ShippingOption, LabeledPrice

POST_SIMP = ShippingOption(
    id='post_simp',
    title='Доставка',
    prices=[
        LabeledPrice(
            'Обычная доставка', 100_00
        ),
    ]
)

POST_FAST = ShippingOption(
    id='post_fast',
    title='Экспресс доставка',
    prices=[
        LabeledPrice(
            'Ускоренная доставка', 300_00
        ),
    ]
)