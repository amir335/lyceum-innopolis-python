from aiogram import types

import config
from loader import dp
from keyboards import game_markup, mmo_kb, rpg_kb, action_kb
from handlers.payments.items import cs, far_cry, rdr, dota
from peewee_db import *
from handlers.payments.shipping import POST_FAST, POST_SIMP

@dp.message_handler(commands=['menu'])
async def get_list(msg: types.Message):
    await msg.answer('Выберите жанр игры:', reply_markup=game_markup)


@dp.callback_query_handler(text='action')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.message.edit_text('выберите игру', reply_markup=action_kb)

@dp.callback_query_handler(text='mmo')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.message.edit_text('выберите игру', reply_markup=mmo_kb)


@dp.callback_query_handler(text='rpg')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.message.edit_text('выберите игру', reply_markup=rpg_kb)


@dp.callback_query_handler(text='back')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.message.edit_text('Выберите жанр игры:', reply_markup=game_markup)

# invoices

@dp.callback_query_handler(text='cs')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **cs.create_invoices(), provider_token=config.PROVIDER_PAYMASTER_TOKEN, payload='cs')

@dp.callback_query_handler(text='fc')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **far_cry.create_invoices(), provider_token=config.PROVIDER_PAYMASTER_TOKEN, payload='far_cry')

@dp.callback_query_handler(text='rdr')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **rdr.create_invoices(), provider_token=config.PROVIDER_PAYMASTER_TOKEN, payload='rdr')

@dp.callback_query_handler(text='dota')
async def awp_callback(call: types.CallbackQuery):
    await call.answer('ready')
    await call.bot.send_invoice(call.from_user.id, **dota.create_invoices(), provider_token=config.PROVIDER_PAYMASTER_TOKEN, payload='dota')


# shipping

@dp.shipping_query_handler()
async def choose_shipping(shipping_query: types.ShippingQuery):
    await shipping_query.bot.answer_shipping_query(
        shipping_query_id=shipping_query.id,
        shipping_options=[
            POST_SIMP,
            POST_FAST,
        ],
        ok=True,
    )

# chek

@dp.pre_checkout_query_handler()
async def pre_checkout(chekout: types.PreCheckoutQuery):
    await chekout.bot.answer_pre_checkout_query(chekout.id, ok=True)
    await chekout.bot.send_message(chekout.from_user.id, 'спасибо за покупку.')

@dp.message_handler(content_types=types.ContentType.SUCCESSFUL_PAYMENT)
async def process_pay(msg: types.Message):
    payload = msg.successful_payment.invoice_payload
    with db:
        try:
            client = Sub.get(Sub.id == msg.from_user.id)
            client.games = f'{client.games}, {payload}'
            client.save()
        except:
            Sub.create(id=msg.from_user.id, games=payload)


