from dataclasses import dataclass
from typing import List

from aiogram.types import LabeledPrice

import config

@dataclass()
class Item():
    title: str
    description: str
    start_parameter: str
    currency: str
    prices: List[LabeledPrice]
    provider_data: dict = None
    photo_url: str = None
    photo_size: int = None
    photo_width: int = None
    photo_height: int = None
    need_name: bool = False
    need_phone_number: bool = False
    need_email: bool = False
    need_shipping_address: bool = False
    send_phone_number_to_provider: bool = False
    send_email_to_provider: bool = False
    is_flexible: bool = False
    # provider_token: str = config.PROVIDER_PAYMASTER_TOKEN

    def create_invoices(self):
        return self.__dict__

cs = Item(
    title='CS:GO',
    description=(
        'Игра CS:GO'
    ),
    currency='RUB',
    prices=[
        LabeledPrice(
            label='CS:GO',
            amount=500_00
        ),
    ],
    start_parameter='create_invoice_cs',
    photo_url="https://cdn.cloudflare.steamstatic.com/steam/apps/730/capsule_616x353.jpg?t=1683566799",
    photo_size=600,
    need_shipping_address=True,
    is_flexible=True

)

far_cry = Item(
    title='Far Cry 3',
    description=(
        'Очень атмосферная игра, прошел ее'
    ),
    currency='RUB',
    prices=[
        LabeledPrice(
            label='CS:GO',
            amount=20_000_00
        ),
    ],
    start_parameter='create_invoice_fc',
    photo_url="https://upload.wikimedia.org/wikipedia/ru/thumb/a/a0/Far_Cry_3_Box_Art_PC.jpeg/640px-Far_Cry_3_Box_Art_PC.jpeg",
    photo_size=600,
    need_shipping_address=True,
    is_flexible=True

)

dota = Item(
    title='Dota 2',
    description=(
        'легендарня игра, которая у меня даже не тянет)'
    ),
    currency='RUB',
    prices=[
        LabeledPrice(
            label='Dota 2',
            amount=300_00
        ),
    ],
    start_parameter='create_invoice_dota',
    photo_url="https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg",
    photo_size=600,
    need_shipping_address=True,
    is_flexible=True

)

rdr = Item(
    title='Red Dead Redemption 2',
    description=(
        'Игра про дикий запад'
    ),
    currency='RUB',
    prices=[
        LabeledPrice(
            label='RDR 2',
            amount=700_00
        ),
    ],
    start_parameter='create_invoice_rdr',
    photo_url='https://www.overclockers.ua/news/games/125760-rdr2-steam.jpg',
    photo_size=600,
    need_shipping_address=True,
    is_flexible=True
)

# subscribes items

# basic = Item(
#     title='basic',
#     description=(
#         'базовая подписка'
#     ),
#     currency='RUB',
#     prices=[
#         LabeledPrice(
#             label='подписка',
#             amount=100_00
#         ),
#     ],
#     start_parameter='create_invoice_basic_subs',
#     photo_size=600,
#     is_flexible=True
# )
#
# premium = Item(
#     title='premium',
#     description=(
#         'premium подписка'
#     ),
#     currency='RUB',
#     prices=[
#         LabeledPrice(
#             label='подписка',
#             amount=150_00
#         ),
#     ],
#     start_parameter='create_invoice_premium_subs',
#     photo_size=600,
#     is_flexible=True
# )
#
# vip = Item(
#     title='vip',
#     description=(
#         'vip подписка'
#     ),
#     currency='RUB',
#     prices=[
#         LabeledPrice(
#             label='подписка',
#             amount=200_00
#         ),
#     ],
#     start_parameter='create_invoice_vip_subs',
#     photo_size=600,
#     is_flexible=True
# )