import sqlite3
from aiogram import types
from loader import dp



@dp.message_handler(text='🔘клик🔘')
async def button_click(msg: types.Message):
    with sqlite3.connect('database.db') as connection:
        cur = connection.cursor()
        clicks = cur.execute(f"SELECT clicks_count FROM user WHERE user_id={msg.from_user.id}").fetchone()[0]
        cur.execute(f"UPDATE user SET clicks_count = ? WHERE user_id = ?", (clicks+1, msg.from_user.id))
        connection.commit()
    await msg.reply(f'(+1) Общее кол-во кликов: {clicks+1}')