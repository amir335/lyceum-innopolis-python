from aiogram import types
from loader import dp
from keyboards import film_kb

@dp.message_handler(commands='film')
async def film_command(msg: types.Message):
    await msg.answer('Советую данный фильм\n'
                    '                   ↓', reply_markup=film_kb)