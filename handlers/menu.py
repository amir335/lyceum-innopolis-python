from aiogram import types
from loader import dp
from aiogram.dispatcher import FSMContext
import sqlite3
from aiogram.dispatcher.filters.state import State, StatesGroup

from keyboards import menu_soups_kb, menu_2e_kb, menu_1kb, menu_callback


@dp.message_handler(text='меню')
async def menu_btn(msg: types.Message):
    await msg.answer('сделайте заказ', reply_markup=menu_1kb)

@dp.callback_query_handler(menu_callback.filter(action='close'))
async def close_btn(callback: types.CallbackQuery):
    await callback.message.edit_text(f'{callback.message.text}', reply_markup=None)

@dp.callback_query_handler(menu_callback.filter(action='1bludo'))
async def close_btn(callback: types.CallbackQuery):
    await callback.message.edit_text(f'{callback.message.text}', reply_markup=menu_soups_kb)

@dp.callback_query_handler(menu_callback.filter(action='2bludo'))
async def close_btn(callback: types.CallbackQuery):
    await callback.message.edit_text(f'{callback.message.text}', reply_markup=menu_2e_kb)

@dp.callback_query_handler(menu_callback.filter(action='back'))
async def close_btn(callback: types.CallbackQuery):
    await callback.message.edit_text(f'{callback.message.text}', reply_markup=menu_1kb)

@dp.callback_query_handler(menu_callback.filter(action=['borch','lapsha','pure','makaroni']))
async def close_btn(callback: types.CallbackQuery, callback_data: dict):
    await callback.message.edit_text(f'{callback.message.text} \n + {callback_data["name"]}', reply_markup=menu_1kb)

