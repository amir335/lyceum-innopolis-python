from aiogram import types

from filters.custom import IsAdmin, IsGroup
from loader import dp

@dp.message_handler(content_types=types.ContentType.NEW_CHAT_MEMBERS)
async def new_member(msg: types.Message):
    await msg.answer(f'привет, {msg.new_chat_members[0].full_name}!')

@dp.message_handler(content_types=types.ContentType.LEFT_CHAT_MEMBER)
async def new_member(msg: types.Message):
    await msg.answer(f'пока, {msg.left_chat_member.full_name}!')