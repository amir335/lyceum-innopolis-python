from aiogram import types
from loader import dp

@dp.message_handler(commands=['about'])
async def about_command(msg: types.Message):
    await msg.answer(f'<b>Здравствуйте.</b>\n'
                     f'от этой команды нет смысла, ведь я добавил описание в description бота) '
                     f'Поэтому я выведу информацию о вас:\n'
                     f'<b>id</b>: {msg.from_user.id}\n'
                     f'<b>alis</b>: {msg.from_user.mention}\n'
                     f'<b>nickname</b>: {msg.from_user.full_name}')