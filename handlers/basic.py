from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton

from aiogram import types

from loader import dp

from keyboards import greet_kb

@dp.message_handler(commands=['start'])
async def process_start_command(msg: types.Message):
    await msg.reply(f'{msg.from_user.first_name}, вас приветсвует Елисей бот!', reply_markup=greet_kb)


@dp.message_handler(commands=['help'])
async def command_help(msg: types.Message):
    await msg.answer('не можем вам помочь:(')