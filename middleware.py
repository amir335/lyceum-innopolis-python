import sqlite3
import logging
from aiogram import types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware

class SomeMiddleware(BaseMiddleware):
    async def on_process_message(self, message: types.Message, data: dict):
        with sqlite3.connect('database.db') as connection:
            cur = connection.cursor()
            user = cur.execute(f"SELECT * FROM user WHERE user_id={message.from_user.id}").fetchone()
            if not user:
                logging.info(f'Создание нового пользователя: {message.from_user.username} {message.from_user.id}')

                msg_data = (message.from_user.id, message.from_user.username, 0)
                cur.execute(f"INSERT INTO user(user_id, username, clicks_count) VALUES (?,?,?)", msg_data)

                connection.commit()
    async def on_pre_process_message(self, msg: types.Message, data: dict):
        with sqlite3.connect('database.db') as connection:
            cur = connection.cursor()
            user = cur.execute(f"SELECT * FROM black_list WHERE user_id={msg.from_user.id}").fetchone()
            if user and msg.text != '/un_block':
                await msg.answer('вы в чс')
                raise CancelHandler('вы в чс')