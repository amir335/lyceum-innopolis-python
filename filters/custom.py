from aiogram.dispatcher.filters import BoundFilter
from aiogram import types

class IsAdmin(BoundFilter):
    async def check(self, msg: types.Message):
        member = await msg.chat.get_member(msg.from_user.id)
        return member.is_chat_admin()


class IsGroup(BoundFilter):
    async def check(self,message:types.Message):
        return message.chat.type in (types.ChatType.GROUP, types.ChatType.SUPERGROUP, )