import sqlite3
from peewee import *
db = SqliteDatabase('subs.db')

class Sub(Model):
    id = PrimaryKeyField()
    games = CharField()
    class Meta:
        database = db
        db_table = 'clients'

