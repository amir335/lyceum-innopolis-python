import sqlite3
def create_table():
    with sqlite3.connect('../save/database.db') as connection:
        cur = connection.cursor()
        cur.execute(
            """
               CREATE TABLE IF NOT EXISTS user(
               user_id INT PRIMARY KEY,
               username TEXT,
               clicks_count INT);
            """
        )
        cur.execute(
            """
               CREATE TABLE IF NOT EXISTS films(
               user_id INT PRIMARY KEY,
               film TEXT);
            """
        )
        cur.execute(
            """
               CREATE TABLE IF NOT EXISTS black_list(
               user_id INT PRIMARY KEY);
            """
        )
        connection.commit()

def set_film(film, id):
    with sqlite3.connect('../save/database.db') as conn:
        cur = conn.cursor()
        user = cur.execute("""select film from films where user_id=?""", (id,)).fetchone()
        if not user:
            cur.execute("""insert into films(user_id, film) values (?, ?)""", (id, film)).fetchone()
        else:
            user = user[0]
            user = list(user.split(' '))
            user.append(film)
            film = ' '.join(user)
            cur.execute("""update films set film=? where user_id=?""", (film, id)).fetchone()
        conn.commit()

def chek_film(id):
    with sqlite3.connect('../save/database.db') as conn:
        cur = conn.cursor()
        user = cur.execute("""select film from films where user_id=?""", (id,)).fetchone()
        if user:
            cur.execute("""delete from films where user_id=?""", (id,)).fetchone()
        conn.commit()


