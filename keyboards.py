from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

button_repeat = KeyboardButton('🔄спам(повтор)🔄')
button_click = KeyboardButton('🔘клик🔘')
button_ball = KeyboardButton('🦊floof🦊')
button_hi = KeyboardButton('🏀basketball🏀')
button_try = KeyboardButton('попытка(try)')
greet_kb = ReplyKeyboardMarkup(resize_keyboard=True, input_field_placeholder="bot from amir")
greet_kb.add(button_hi).add(button_try).add(button_ball).add(button_click).add(button_repeat)
greet_kb.add(KeyboardButton('Да'), KeyboardButton('Нет'))
greet_kb.add(KeyboardButton('Отправить свою локацию', request_location=True))
greet_kb.add(KeyboardButton('меню'))

btn_film = InlineKeyboardButton('фильм', url='https://hd-4.videobox.onl/2136-bojcovskij-klub-film-2000.html')
film_kb = InlineKeyboardMarkup().add(btn_film)

menu_callback = CallbackData('menu_d', 'action', 'name')
btn_1e = InlineKeyboardButton('первое', callback_data=menu_callback.new(action='1bludo', name='1-е'))
btn_2e = InlineKeyboardButton('второе', callback_data=menu_callback.new(action='2bludo', name='2-е'))
btn_close = InlineKeyboardButton('закрыть', callback_data=menu_callback.new(action='close', name='закрыть'))
menu_1kb = InlineKeyboardMarkup().add(btn_1e, btn_2e).add(btn_close)

btn_back = InlineKeyboardButton('назад', callback_data=menu_callback.new(action='back', name='назад'))

btn_borch = InlineKeyboardButton('борщ', callback_data=menu_callback.new(action='borch', name='борщ'))
btn_lapsha = InlineKeyboardButton('суп-лапша', callback_data=menu_callback.new(action='lapsha', name='суп-лапша'))

menu_soups_kb = InlineKeyboardMarkup().add(btn_borch, btn_lapsha).add(btn_back).add(btn_close)

btn_pure = InlineKeyboardButton('пюре', callback_data=menu_callback.new(action='pure', name='макароны'))
btn_makaroni = InlineKeyboardButton('макароны', callback_data=menu_callback.new(action='makaroni', name='макароны'))

menu_2e_kb = InlineKeyboardMarkup().add(btn_makaroni, btn_pure).add(btn_back).add(btn_close)


fbtn_1 = InlineKeyboardButton('фантастика', callback_data='fbtn_fantasy')
fbtn_2 = InlineKeyboardButton('драма', callback_data='fbtn_drama')
fbtn_3 = InlineKeyboardButton('комедия', callback_data='fbtn_comedia')
fbtn_4 = InlineKeyboardButton('закончить', callback_data='close')
fmarkup = InlineKeyboardMarkup(row_width=1).add(fbtn_3, fbtn_2, fbtn_1, fbtn_4)


btn_skin1 = InlineKeyboardButton('AWP', callback_data='awp_skin')
btn_skin2 = InlineKeyboardButton('Knife', callback_data='knife_skin')
btn_skin3 = InlineKeyboardButton('P250', callback_data='p250_skin')
skin_markup = InlineKeyboardMarkup(row_width=1).add(btn_skin1, btn_skin2, btn_skin3)


btn_qiwi = InlineKeyboardButton('qiwi', callback_data='qiwi')
btn_paymaster = InlineKeyboardButton('paymaster', callback_data='paymaster')
pay_method_markup = InlineKeyboardMarkup(row_width=1).add(btn_qiwi, btn_paymaster)

btn_skin1_2 = InlineKeyboardButton('AWP', callback_data='awp_skin2')
btn_skin2_2 = InlineKeyboardButton('Knife', callback_data='knife_skin2')
btn_skin3_2 = InlineKeyboardButton('P250', callback_data='p250_skin2')
skin_markup_2 = InlineKeyboardMarkup(row_width=1).add(btn_skin1_2, btn_skin2_2, btn_skin3_2)

btn_basic = InlineKeyboardButton('basic', callback_data='basic')
btn_premium = InlineKeyboardButton('premium', callback_data='premium')
btn_vip = InlineKeyboardButton('vip', callback_data='vip')
subs_markup = InlineKeyboardMarkup(row_width=1).add(btn_vip, btn_premium, btn_basic)

btn_action = InlineKeyboardButton('action', callback_data='action')
btn_mmo = InlineKeyboardButton('mmo', callback_data='mmo')
btn_rpg = InlineKeyboardButton('rpg', callback_data='rpg')
game_markup = InlineKeyboardMarkup(row_width=1).add(btn_action, btn_mmo, btn_rpg)

back_btn = InlineKeyboardButton('Назад', callback_data='back')
action_kb = InlineKeyboardMarkup(row_width=1).add(InlineKeyboardButton('cs:go', callback_data='cs')).add(InlineKeyboardButton('far cry 3', callback_data='fc')).add(back_btn)
mmo_kb = InlineKeyboardMarkup(row_width=1).add(InlineKeyboardButton('Dota 2', callback_data='dota')).add(back_btn)
rpg_kb = InlineKeyboardMarkup(row_width=1).add(InlineKeyboardButton('RDR 2', callback_data='rdr')).add(back_btn)