# https://gitlab.com/amir335/lyceum-innopolis-python.git - гитлаб
# дз 5 и 6

from peewee_db import *
import logging


import config
from loader import dp
from middleware import SomeMiddleware





async def on_startup(dp):
    logging.info('starting...')
    await dp.bot.set_my_commands(config.COMMANDS)


async def on_shotdown(dp):
    logging.info('ending...')




if __name__ == '__main__':
    from aiogram import executor
    from database import create_table
    from handlers import *
    with db:
        db.create_tables([Sub])
    create_table()
    dp.middleware.setup(SomeMiddleware())
    executor.start_polling(
        dispatcher=dp,
        skip_updates=True,
        on_startup=on_startup,
        on_shutdown=on_shotdown,
    )