import logging

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from config import TOKEN

logging.basicConfig(level=logging.INFO)

# PROXY_URL = "http://proxy.server:3128"

bot = Bot(
    token=TOKEN,
    parse_mode='HTML',
    # proxy=PROXY_URL
)
dp = Dispatcher(bot, storage=MemoryStorage())