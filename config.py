import os
from aiogram import types
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


TOKEN = os.getenv('TOKEN')

NGROK = os.getenv('NGROK', '')

PROVIDER_PAYMASTER_TOKEN = os.getenv('PROVIDER_PAYMASTER_TOKEN')

PROVIDER_UKASSA_TOKEN = os.getenv('PROVIDER_UKASSA_TOKEN')

# настройки webhook
WEBHOOK_PATH = ''
WEBHOOK_URL = f'{NGROK}{WEBHOOK_PATH}'

# настройки веб сервера
WEBAPP_HOST = '127.0.0.1'  # or ip
WEBAPP_PORT = 8000


if not TOKEN:
    exit('Error: no token provided')


COMMANDS = [
            types.BotCommand('about', 'о вас и немного о боте'),
            types.BotCommand('un_block', 'Удалить вас из чс'),
            types.BotCommand('to_black_list', 'Добавить вас в черный список'),
            types.BotCommand('start', 'начать общение с ботом'),
            types.BotCommand('help', 'помощь и справка по боту'),
            types.BotCommand('admin_msg', 'сделать рассылку(доступно только админу)'),
            types.BotCommand('film', 'Ссылка на фильм'),
            types.BotCommand('poll', 'опрос по жанрам фильмов'),
            types.BotCommand('get_rate', 'курс рубля по юань'),
            types.BotCommand('ban', 'для админа группы'),
            types.BotCommand('skins', 'купить скины'),
            types.BotCommand('menu', 'купить подписку'),
        ]

BAD_WORDS = ['***', 'ban me', 'dumb']